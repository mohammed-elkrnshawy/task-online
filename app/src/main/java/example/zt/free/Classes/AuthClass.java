package example.zt.free.Classes;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import example.zt.free.Activities.HomeActivity;
import example.zt.free.Activities.LoginActivity;

import static android.content.Context.MODE_PRIVATE;

public class AuthClass {
    private Context context;
    private Dialog dialogProgress;
    private boolean isFound;
    private Salesman salesman;
    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

    public AuthClass(Context context) {
        this.context = context;
    }

    private String EncryptPassword(String s) {
        String encrypted = "";
        encrypted = EncryptString.md5Encrypt(s);
        return encrypted;
    }

    public void Search(final String Mail, String Password) {
        isFound = false;
        dialogProgress = SharedUtils.ShowWaiting(context, dialogProgress);
        dialogProgress.show();

        final String finalPassword = EncryptPassword(Password);

        salesman = new Salesman();
        mDatabase.child(Constants.Salesman_Root).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    for (DataSnapshot snapshot :dataSnapshot.getChildren()){
                        if (snapshot.child("salesman_Mail").getValue().equals(Mail)
                                &&snapshot.child("salesman_Password").getValue().equals(finalPassword)) {
                            salesman = snapshot.getValue(Salesman.class);
                            salesman.setSalesman_UID(snapshot.getKey());
                            isFound=true;
                            break;
                        }
                    }
                }else {
                    isFound=false;
                    Toast.makeText(context, "لا يوجد بيانات", Toast.LENGTH_SHORT).show();
                }
                if (isFound){
                    SharedPreferencesPut(salesman.getSalesman_UID());
                    Intent intent = new Intent(context, HomeActivity.class);
                    intent.putExtra("SalesmanObject", salesman);
                    context.startActivity(intent);
                    ((Activity) context).finishAffinity();
                }else {
                    Toast.makeText(context, "من فضلك تأكد من البيانات", Toast.LENGTH_SHORT).show();
                }

                dialogProgress.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                dialogProgress.dismiss();
            }
        });
    }

    private void SharedPreferencesPut(String UserUID) {
        //SharedPreferences.Editor editor = context.getSharedPreferences(context.getApplicationContext().getPackageName(), MODE_PRIVATE).edit();
        SharedPreferences.Editor editor = context.getSharedPreferences(context.getPackageName(), MODE_PRIVATE).edit();
        editor.putString("Token", UserUID);
        editor.putBoolean("isLogin", true);
        editor.apply();
    }

    public void Login(final String token) {
        isFound = false;

        salesman = new Salesman();
        mDatabase.child(Constants.Salesman_Root).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    for (DataSnapshot snapshot :dataSnapshot.getChildren()){
                        Log.e("QQQQQQQQQQQQQQQQQQQQQQ",token);
                        if (snapshot.getKey().equals(token)) {
                            salesman = snapshot.getValue(Salesman.class);
                            salesman.setSalesman_UID(token);
                            isFound=true;
                            break;
                        }
                    }
                }else {
                    isFound=false;
                    context.startActivity(new Intent(context, LoginActivity.class));
                }

                if (isFound){
                    SharedPreferencesPut(salesman.getSalesman_UID());
                    Intent intent = new Intent(context, HomeActivity.class);
                    intent.putExtra("SalesmanObject", salesman);
                    context.startActivity(intent);
                    ((Activity) context).finishAffinity();
                }else {
                    context.startActivity(new Intent(context, LoginActivity.class));
                    ((Activity) context).finishAffinity();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}