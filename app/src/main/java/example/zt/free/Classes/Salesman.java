package example.zt.free.Classes;

import java.io.Serializable;

public class Salesman implements Serializable {
    private String Salesman_UID;
    private String Salesman_Name;
    private String Salesman_Password;
    private String Salesman_Mail;

    public String getSalesman_UID() {
        return Salesman_UID;
    }

    public void setSalesman_UID(String salesman_UID) {
        Salesman_UID = salesman_UID;
    }

    public String getSalesman_Name() {
        return Salesman_Name;
    }

    public void setSalesman_Name(String salesman_Name) {
        Salesman_Name = salesman_Name;
    }

    public String getSalesman_Password() {
        return Salesman_Password;
    }

    public void setSalesman_Password(String salesman_Password) {
        Salesman_Password = salesman_Password;
    }

    public String getSalesman_Mail() {
        return Salesman_Mail;
    }

    public void setSalesman_Mail(String salesman_Mail) {
        Salesman_Mail = salesman_Mail;
    }
}
