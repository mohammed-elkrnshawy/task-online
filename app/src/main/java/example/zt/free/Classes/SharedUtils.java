package example.zt.free.Classes;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.ViewGroup;
import android.view.Window;


import example.zt.free.R;

public class SharedUtils {

    public static Dialog ShowWaiting(Context context, Dialog progressDialog) {
        progressDialog = new Dialog(context);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.view_waiting);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        progressDialog.setCancelable(true);
        return progressDialog;
    }

    public static String convertNumber(String s){
        String newString="";
        newString=s.replace("٠","0")
                .replace("١","1")
                .replace("٢","2")
                .replace("٣","3")
                .replace("٤","4")
                .replace("٥","5")
                .replace("٦","6")
                .replace("٧","7")
                .replace("٨","8")
                .replace("٩","9");
        return newString;
    }
}
