package example.zt.free.Classes;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import example.zt.free.Activities.AddActivity;
import example.zt.free.R;

import static android.app.Activity.RESULT_OK;


public class CameraClass {

    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    private Bitmap bitmap;
    private Uri uriFilePath;
    private String mCurrentPhotoPath;
    private Context context;

    public CameraClass(Context context){
        this.context=context;
    }

    public void SelectCameraDialog(){
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {

            int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);


            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                ChooseImageCamera();

            } else {  //  PERMISSION_DENIED


                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA},0);
            }
        } else {

            ChooseImageCamera();
        }
    }

    public void SelectGalleryDialog(){
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {

            int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);


            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                ChooseImageGallery();


            } else {  //  PERMISSION_DENIED
                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            }

        } else {
            ChooseImageGallery();
        }
    }

    private void ChooseImageGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        ((Activity)context).startActivityForResult(Intent.createChooser(intent, context.getResources().getString(R.string.add_photo)), Constants.Gallery);
    }

    private void ChooseImageCamera() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                uriFilePath = FileProvider.getUriForFile(context,
                        "example.zt.free.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriFilePath);
                ((Activity)context).startActivityForResult(takePictureIntent, Constants.Camera);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private String getImagePathFromInputStreamUri(Uri uri) {
        InputStream inputStream = null;
        String filePath = null;

        if (uri.getAuthority() != null) {
            try {
                inputStream = context.getContentResolver().openInputStream(uri); // context needed
                File photoFile = createTemporalFileFrom(inputStream);

                filePath = photoFile.getPath();

            } catch (FileNotFoundException e) {
                // log
            } catch (IOException e) {
                // log
            } finally {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return filePath;
    }

    private File createTemporalFileFrom(InputStream inputStream) throws IOException {
        File targetFile = null;

        if (inputStream != null) {
            int read;
            byte[] buffer = new byte[8 * 1024];

            targetFile = createTemporalFile();
            OutputStream outputStream = new FileOutputStream(targetFile);

            while ((read = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, read);
            }
            outputStream.flush();

            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return targetFile;
    }

    private File createTemporalFile() {
        return new File(context.getExternalCacheDir(), "tempFile.jpg"); // context needed
    }

    private void fixRotate(String photoPath) throws IOException {
        ExifInterface ei = new ExifInterface(photoPath);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);
        switch (orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                bitmap = rotateImage(90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                bitmap = rotateImage(180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                bitmap = rotateImage(270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                break;
        }
    }

    private Bitmap rotateImage(float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public void onPhotoResult(int requestCode, int resultCode, Intent data, ImageView circleImageView) {
        if (requestCode == Constants.Gallery) {
            if (data != null) {
                uriFilePath = data.getData();
                mCurrentPhotoPath = getImagePathFromInputStreamUri(uriFilePath);
                setImage(circleImageView);
            }

        } else if (requestCode == Constants.Camera) {
            setImage(circleImageView);
        }
    }

    private void setImage(ImageView imgPhoto) {
        bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, new BitmapFactory.Options());
        try {
            fixRotate(mCurrentPhotoPath);

        } catch (IOException e) {
            e.printStackTrace();
        }

        imgPhoto.setImageBitmap(bitmap);
    }

    public void uploadPhoto(final OrderClass orderClass) {
        final String[] Url = new String[1];
        if (uriFilePath != null) {
            final StorageReference ref = storageReference.child("images/" + UUID.randomUUID().toString());
            ref.putFile(uriFilePath)
                    .continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }
                            return ref.getDownloadUrl();
                        }
                    })
                    .addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                Uri taskResult = task.getResult();
                                orderClass.setImagePath(taskResult.toString());


                                FirebaseDatabase.getInstance().getReference().child(Constants.Order_Root)
                                        .child(orderClass.getOrder_UID()).setValue(orderClass).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()){
                                            Intent intent = ((Activity)context).getIntent();
                                            intent.putExtra("OrderObject", orderClass);
                                            ((Activity)context).setResult(RESULT_OK, intent);
                                            ((Activity)context).finish();
                                            Toast.makeText(context, "تم الحفظ بنجاح", Toast.LENGTH_SHORT).show();
                                        }else {
                                            Toast.makeText(context, "تم حفظ العملية اوفلاين", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });


                            }else {
                                Toast.makeText(context, "من فضلك حاةل مرة احري", Toast.LENGTH_SHORT).show();
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("EEEEEEEEEEEEEEEEEEEEEEE",e.getMessage());
                        }
                    });
        }
    }
}
