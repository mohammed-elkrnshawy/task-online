package example.zt.free.Classes;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

import example.zt.free.Activities.ShowDetailsActivity;
import example.zt.free.R;


public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyHolder> {


    private List<OrderClass> listdata;
    private Context context;
    private int selected_index=0;

    public OrderAdapter(List<OrderClass> listdata, Context context) {
        this.listdata = listdata;
        this.context = context;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_order,parent,false);
        MyHolder myHolder = new MyHolder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(MyHolder holder, final int position) {

        holder.txt_ID.setText(listdata.get(position).getOrder_ID()+"");
        holder.txt_Date.setText(listdata.get(position).getOrder_Time());
        holder.txt_Fullname.setText(listdata.get(position).getFullName());

        holder.linearParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, ShowDetailsActivity.class);
                intent.putExtra("OrderObject",listdata.get(position));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    class MyHolder extends RecyclerView.ViewHolder{
       private CardView linearParent;
       private TextView txt_Fullname,txt_Date,txt_ID;


        public MyHolder(View itemView) {
            super(itemView);

            linearParent=itemView.findViewById(R.id.linearParent);
            txt_Fullname=itemView.findViewById(R.id.txt_Fullname);
            txt_Date=itemView.findViewById(R.id.txt_Date);
            txt_ID=itemView.findViewById(R.id.txt_ID);
        }
    }

}
