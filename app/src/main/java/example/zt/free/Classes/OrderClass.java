package example.zt.free.Classes;

import java.io.Serializable;

public class OrderClass implements Serializable {
    private int Order_ID;
    private String Order_UID;
    private String FullName;
    private String Phone;
    private String ID_Type;
    private String ID_Number;
    private String Home_Type;
    private String Moderia;
    private String Street;
    private String Wife_Name;
    private String Number;
    private String Notes;
    private String Salesman_UID;
    private String Salesman_Name;
    private String Order_Time;
    private String Notes2;
    private String Area;
    private String imagePath;

    public String getSalesman_Name() {
        return Salesman_Name;
    }

    public void setSalesman_Name(String salesman_Name) {
        Salesman_Name = salesman_Name;
    }

    public int getOrder_ID() {
        return Order_ID;
    }

    public void setOrder_ID(int order_ID) {
        Order_ID = order_ID;
    }

    public String getOrder_UID() {
        return Order_UID;
    }

    public void setOrder_UID(String order_UID) {
        Order_UID = order_UID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getID_Type() {
        return ID_Type;
    }

    public void setID_Type(String ID_Type) {
        this.ID_Type = ID_Type;
    }

    public String getID_Number() {
        return ID_Number;
    }

    public void setID_Number(String ID_Number) {
        this.ID_Number = ID_Number;
    }

    public String getHome_Type() {
        return Home_Type;
    }

    public void setHome_Type(String home_Type) {
        Home_Type = home_Type;
    }

    public String getModeria() {
        return Moderia;
    }

    public void setModeria(String moderia) {
        Moderia = moderia;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getWife_Name() {
        return Wife_Name;
    }

    public void setWife_Name(String wife_Name) {
        Wife_Name = wife_Name;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getSalesman_UID() {
        return Salesman_UID;
    }

    public void setSalesman_UID(String salesman_UID) {
        Salesman_UID = salesman_UID;
    }

    public String getOrder_Time() {
        return Order_Time;
    }

    public void setOrder_Time(String order_Time) {
        Order_Time = order_Time;
    }

    public String getNotes2() {
        return Notes2;
    }

    public void setNotes2(String notes2) {
        Notes2 = notes2;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
