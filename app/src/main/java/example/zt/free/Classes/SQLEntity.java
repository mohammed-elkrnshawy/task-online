package example.zt.free.Classes;

public class SQLEntity {
    public static final String TABLE_NAME = "Orders";
    public static final String _ID = "ID";
    public static final String COLUMN_NAME_fullName = "FullName";
    public static final String COLUMN_NAME_home_Type = "Home_Type";
    public static final String COLUMN_NAME_id_Number = "id_Number";
    public static final String COLUMN_NAME_id_Type   = "id_Type";
    public static final String COLUMN_NAME_moderia   = "moderia";
    public static final String COLUMN_NAME_notes     = "notes";
    public static final String COLUMN_NAME_notes2     = "notes2";
    public static final String COLUMN_NAME_number    = "number";
    public static final String COLUMN_NAME_order_Time= "order_Time";
    public static final String COLUMN_NAME_phone     = "phone";
    public static final String COLUMN_NAME_street    = "street";
    public static final String COLUMN_NAME_area    = "area";
    public static final String COLUMN_NAME_wife_Name = "wife_Name";
}
