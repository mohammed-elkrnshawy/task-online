package example.zt.free.Classes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class SQLHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Khatwa.db";



    public SQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + SQLEntity.TABLE_NAME + " (" +
                        SQLEntity._ID + " INTEGER PRIMARY KEY," +
                        SQLEntity.COLUMN_NAME_fullName + " TEXT," +
                        SQLEntity.COLUMN_NAME_home_Type + " TEXT," +
                        SQLEntity.COLUMN_NAME_id_Number + " TEXT," +
                        SQLEntity.COLUMN_NAME_id_Type + " TEXT," +
                        SQLEntity.COLUMN_NAME_moderia + " TEXT," +
                        SQLEntity.COLUMN_NAME_notes + " TEXT," +
                        SQLEntity.COLUMN_NAME_number + " TEXT," +
                        SQLEntity.COLUMN_NAME_order_Time + " TEXT," +
                        SQLEntity.COLUMN_NAME_phone + " TEXT," +
                        SQLEntity.COLUMN_NAME_street + " TEXT," +
                        SQLEntity.COLUMN_NAME_wife_Name + " TEXT," +
                        SQLEntity.COLUMN_NAME_area + " TEXT," +
                        SQLEntity.COLUMN_NAME_notes2 + " TEXT)";
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + SQLEntity.TABLE_NAME;
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
