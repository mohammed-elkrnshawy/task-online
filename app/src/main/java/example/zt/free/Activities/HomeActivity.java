package example.zt.free.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.zt.free.Classes.Constants;
import example.zt.free.Classes.OrderAdapter;
import example.zt.free.Classes.OrderClass;
import example.zt.free.Classes.SQLEntity;
import example.zt.free.Classes.SQLHelper;
import example.zt.free.Classes.Salesman;
import example.zt.free.Classes.SharedUtils;
import example.zt.free.R;

public class HomeActivity extends AppCompatActivity {
    private Salesman salesmanObject;
    private DatabaseReference mReference;
    private RecyclerView.LayoutManager layoutManager;
    private OrderAdapter orderAdapter;
    private List<OrderClass> donateClassList;
    private OrderClass orderClass;
    private Dialog dialog;

    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.recycle)
    RecyclerView recyclerView;
    @BindView(R.id.relativeEmpty)
    RelativeLayout relativeEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        getIntentData();
        setToolBar();
        initComponents();
        ReadOrder();
        getOrderID();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, AddActivity.class);
                intent.putExtra("SalesmanObject", salesmanObject);
                startActivity(intent);
            }
        });
    }

    private void getOrderID() {
        FirebaseDatabase.getInstance().getReference().child(Constants.Order_Root)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Constants.order_ID = (int) dataSnapshot.getChildrenCount();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (!bundle.isEmpty()) {
            salesmanObject = (Salesman) bundle.get("SalesmanObject");
        }
    }

    private void initComponents() {
        mReference = FirebaseDatabase.getInstance().getReference();
        layoutManager = new LinearLayoutManager(this);
        donateClassList = new ArrayList<>();
        recyclerView.setLayoutManager(layoutManager);
        orderAdapter = new OrderAdapter(donateClassList, this);
        recyclerView.setAdapter(orderAdapter);
    }

    private void ReadOrder() {
        mReference.child(Constants.Order_Root)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        donateClassList.clear();
                        if (dataSnapshot.exists()) {
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                if (snapshot.child("salesman_UID").getValue().equals(salesmanObject.getSalesman_UID())) {
                                    orderClass = snapshot.getValue(OrderClass.class);
                                    donateClassList.add(orderClass);
                                }
                            }
                        }
                        if (donateClassList.size() != 0) {
                            relativeEmpty.setVisibility(View.GONE);
                        } else {
                            relativeEmpty.setVisibility(View.VISIBLE);
                        }

                        Collections.reverse(donateClassList);
                        orderAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void setToolBar() {
        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.bar_search);
        //getSupportActionBar().setElevation(0);
        View view = getSupportActionBar().getCustomView();
        TextView txttitle = view.findViewById(R.id.edt_Search);
        TextView edt_sync = view.findViewById(R.id.edt_sync);
        TextView edt_logout = view.findViewById(R.id.edt_logout);

        txttitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, SearchActivity.class);
                intent.putExtra("SalesmanObject", salesmanObject);
                startActivity(intent);
            }
        });

        edt_sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkConnected())
                    syncData();
                else
                    Toast.makeText(HomeActivity.this, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
            }
        });

        edt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    private void logout() {
        SharedPreferences.Editor editor = getSharedPreferences(getPackageName(), MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();

        Intent intent=new Intent(this, SplashActivity.class);
        startActivity(intent);
        finishAffinity();
    }

    private void syncData() {
        OrderClass aClass=new OrderClass();
        SQLHelper dbHelper = new SQLHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                SQLEntity._ID,
                SQLEntity.COLUMN_NAME_fullName,
                SQLEntity.COLUMN_NAME_home_Type,
                SQLEntity.COLUMN_NAME_id_Number,
                SQLEntity.COLUMN_NAME_id_Type,
                SQLEntity.COLUMN_NAME_moderia,
                SQLEntity.COLUMN_NAME_notes,
                SQLEntity.COLUMN_NAME_number,
                SQLEntity.COLUMN_NAME_order_Time,
                SQLEntity.COLUMN_NAME_phone,
                SQLEntity.COLUMN_NAME_street,
                SQLEntity.COLUMN_NAME_wife_Name,
                SQLEntity.COLUMN_NAME_area,
                SQLEntity.COLUMN_NAME_notes2
        };

        Cursor cursor = db.query(
                SQLEntity.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                null               // The sort order
        );

        if (cursor.getCount()==0)
            Toast.makeText(this, "لا يوجد داتا", Toast.LENGTH_SHORT).show();


        while (cursor.moveToNext()) {
            String Key=FirebaseDatabase.getInstance().getReference().child(Constants.Order_Root).push().getKey();
            aClass.setOrder_UID(Key);
            aClass.setWife_Name(cursor.getString(cursor.getColumnIndexOrThrow(SQLEntity.COLUMN_NAME_wife_Name)));
            aClass.setStreet(cursor.getString(cursor.getColumnIndexOrThrow(SQLEntity.COLUMN_NAME_street)));
            aClass.setSalesman_UID(salesmanObject.getSalesman_UID());
            aClass.setSalesman_Name(salesmanObject.getSalesman_Name());
            aClass.setID_Number(cursor.getString(cursor.getColumnIndexOrThrow(SQLEntity.COLUMN_NAME_id_Number)));
            aClass.setPhone(cursor.getString(cursor.getColumnIndexOrThrow(SQLEntity.COLUMN_NAME_phone)));
            aClass.setOrder_Time(cursor.getString(cursor.getColumnIndexOrThrow(SQLEntity.COLUMN_NAME_order_Time)));
            aClass.setNumber(cursor.getString(cursor.getColumnIndexOrThrow(SQLEntity.COLUMN_NAME_number)));
            aClass.setNotes(cursor.getString(cursor.getColumnIndexOrThrow(SQLEntity.COLUMN_NAME_notes)));
            aClass.setModeria(cursor.getString(cursor.getColumnIndexOrThrow(SQLEntity.COLUMN_NAME_moderia)));
            aClass.setID_Type(cursor.getString(cursor.getColumnIndexOrThrow(SQLEntity.COLUMN_NAME_id_Type)));
            aClass.setHome_Type(cursor.getString(cursor.getColumnIndexOrThrow(SQLEntity.COLUMN_NAME_home_Type)));
            aClass.setFullName(cursor.getString(cursor.getColumnIndexOrThrow(SQLEntity.COLUMN_NAME_fullName)));
            aClass.setArea(cursor.getString(cursor.getColumnIndexOrThrow(SQLEntity.COLUMN_NAME_area)));
            aClass.setNotes2(cursor.getString(cursor.getColumnIndexOrThrow(SQLEntity.COLUMN_NAME_notes2)));
            int sqlID=cursor.getInt(cursor.getColumnIndexOrThrow(SQLEntity._ID));
            addToFirebase(sqlID+"",db,aClass);
           break;
        }
        cursor.close();
    }

    private void addToFirebase(final String ID, final SQLiteDatabase db, OrderClass orderClass){

        orderClass.setOrder_ID((Constants.order_ID+1));
        dialog= SharedUtils.ShowWaiting(this,dialog);
        dialog.show();
        FirebaseDatabase.getInstance().getReference().child(Constants.Order_Root).child(orderClass.getOrder_UID())
                .setValue(orderClass).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    deleteFromSql(ID,db);
                    Toast.makeText(HomeActivity.this, "تم الحفظ بنجاح", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }else {
                    Toast.makeText(HomeActivity.this, "تم حفظ العملية اوفلاين", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void deleteFromSql(String ID,SQLiteDatabase db){
        // Define 'where' part of query.
        String selection = SQLEntity._ID + " = ?";
// Specify arguments in placeholder order.
        String[] selectionArgs = { ID };
// Issue SQL statement.
        int deletedRows = db.delete(SQLEntity.TABLE_NAME, selection, selectionArgs);
        dialog.dismiss();
        syncData();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}
