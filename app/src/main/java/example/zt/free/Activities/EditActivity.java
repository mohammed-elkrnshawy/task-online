package example.zt.free.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.zt.free.Classes.CameraClass;
import example.zt.free.Classes.Constants;
import example.zt.free.Classes.OrderClass;
import example.zt.free.Classes.SQLEntity;
import example.zt.free.Classes.SQLHelper;
import example.zt.free.Classes.Salesman;
import example.zt.free.Classes.SharedUtils;
import example.zt.free.R;

public class EditActivity extends AppCompatActivity {
    private boolean isChanged=false;
    private CameraClass cameraClass;
    private Dialog dialog;
    private OrderClass aClass;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btnCamera)
    ImageView btnCamera;
    @BindView(R.id.btnGallery)
    ImageView btnGallery;
    @BindView(R.id.imagePhoto)
    ImageView imagePhoto;

    @BindView(R.id.spinner_IDType)
    Spinner spinner_IDType;
    @BindView(R.id.spinner_Moderia)
    Spinner spinner_Moderia;
    @BindView(R.id.spinner_HomeType)
    Spinner spinner_HomeType;

    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.edtPhone)
    EditText edtPhone;
    @BindView(R.id.edtID)
    EditText edtID;
    @BindView(R.id.edtArea)
    EditText edtArea;
    @BindView(R.id.edtStreet)
    EditText edtStreet;
    @BindView(R.id.edtWifeName)
    EditText edtWifeName;
    @BindView(R.id.edtNumber)
    EditText edtNumber;
    @BindView(R.id.txtNotes)
    EditText txtNotes;
    @BindView(R.id.txtNotes2)
    EditText txtNotes2;
    @BindView(R.id.btn_Next)
    Button btn_Next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        ButterKnife.bind(this);

        initComponents();
        setToolBar();
        getIntentData();
        showData();

        btn_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ValidData();
            }
        });
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraClass.SelectCameraDialog();
            }
        });
        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { cameraClass.SelectGalleryDialog();
            }
        });
    }
    private void initComponents() {
        cameraClass=new CameraClass(this);
        //region ImageLoader
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);
        //endregion
    }

    private void setToolBar(){
        setSupportActionBar(toolbar);
        final LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        if (inflator != null) {
            view = inflator.inflate(R.layout.bar_home, null);
        }
        TextView txttitle=view.findViewById(R.id.title);
        txttitle.setText("تعديل اللب");
        toolbar.addView(view);
    }

    private void getIntentData() {
        Bundle bundle=getIntent().getExtras();
        if (!bundle.isEmpty()){
            aClass=(OrderClass) bundle.get("OrderObject");
        }
    }

    private void showData(){
        ImageLoader.getInstance().displayImage(aClass.getImagePath(),imagePhoto,
                new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        imagePhoto.setImageDrawable(getResources().getDrawable(R.drawable.ic_logo_big));
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        imagePhoto.setImageBitmap(loadedImage);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {

                    }
                });

        edtName.setText(aClass.getFullName());
        edtPhone.setText(aClass.getPhone());
        edtID.setText(aClass.getID_Number());
        edtStreet.setText(aClass.getStreet());
        edtArea.setText(aClass.getArea());
        txtNotes2.setText(aClass.getNotes2());
        edtWifeName.setText(aClass.getWife_Name());
        edtNumber.setText(aClass.getNumber());
        txtNotes.setText(aClass.getNotes());
        txtNotes2.setText(aClass.getNotes2());
    }

    private void ValidData() {
        if(TextUtils.isEmpty(edtName.getText().toString().trim())){
            edtName.setError(getResources().getString(R.string.required));
            edtName.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(edtPhone.getText().toString().trim())){
            edtPhone.setError(getResources().getString(R.string.required));
            edtPhone.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(edtID.getText().toString().trim())){
            edtID.setError(getResources().getString(R.string.required));
            edtID.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(edtArea.getText().toString().trim())){
            edtArea.setError(getResources().getString(R.string.required));
            edtArea.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(edtStreet.getText().toString().trim())){
            edtStreet.setError(getResources().getString(R.string.required));
            edtStreet.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(edtWifeName.getText().toString().trim())){
            edtWifeName.setError(getResources().getString(R.string.required));
            edtWifeName.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(edtNumber.getText().toString().trim())){
            edtNumber.setError(getResources().getString(R.string.required));
            edtNumber.requestFocus();
            return;
        }

        OrderClass orderClass=new OrderClass();
        orderClass.setFullName(edtName.getText().toString().trim());
        orderClass.setHome_Type(spinner_HomeType.getSelectedItem().toString().trim());
        orderClass.setID_Number(edtID.getText().toString().trim());
        orderClass.setID_Type(spinner_IDType.getSelectedItem().toString().trim());
        orderClass.setModeria(spinner_Moderia.getSelectedItem().toString().trim());
        orderClass.setNotes(txtNotes.getText().toString().trim());
        orderClass.setNumber(edtNumber.getText().toString().trim());
        orderClass.setOrder_Time(aClass.getOrder_Time());
        orderClass.setPhone(edtPhone.getText().toString());
        orderClass.setSalesman_UID(aClass.getSalesman_UID());
        orderClass.setSalesman_Name(aClass.getSalesman_Name());
        orderClass.setStreet(edtStreet.getText().toString());
        orderClass.setWife_Name(edtWifeName.getText().toString());
        orderClass.setArea(edtArea.getText().toString().trim());
        orderClass.setNotes2(txtNotes2.getText().toString().trim());
        orderClass.setImagePath(aClass.getImagePath());

        dialog= SharedUtils.ShowWaiting(this,dialog);
        dialog.show();

        orderClass.setOrder_UID(aClass.getOrder_UID());
        orderClass.setOrder_ID(aClass.getOrder_ID());


        if (isNetworkConnected()){
            saveOnline(orderClass);
        }else {
            Toast.makeText(this, "لا يمكن التعديل وفلاين", Toast.LENGTH_SHORT).show();
        }


    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private void saveOnline(final OrderClass orderClass){
        if (isChanged)
            cameraClass.uploadPhoto(orderClass);
        else {
            FirebaseDatabase.getInstance().getReference().child(Constants.Order_Root)
                    .child(orderClass.getOrder_UID()).setValue(orderClass).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()){
                        Intent intent = getIntent();
                        intent.putExtra("OrderObject", orderClass);
                        setResult(RESULT_OK, intent);
                        finish();
                        Toast.makeText(EditActivity.this, "تم الحفظ بنجاح", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(EditActivity.this, "تم حفظ العملية اوفلاين", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode== Activity.RESULT_OK&&(requestCode==Constants.Camera||requestCode==Constants.Gallery)){
            cameraClass.onPhotoResult(requestCode,resultCode,data,imagePhoto);
            isChanged=true;
        }
    }
}