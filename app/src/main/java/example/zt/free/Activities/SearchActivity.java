package example.zt.free.Activities;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.zt.free.Classes.Constants;
import example.zt.free.Classes.OrderAdapter;
import example.zt.free.Classes.OrderClass;
import example.zt.free.Classes.Salesman;
import example.zt.free.Classes.SharedUtils;
import example.zt.free.R;

public class SearchActivity extends AppCompatActivity {
    private Salesman salesmanObject;
    private DatabaseReference mReference;
    private RecyclerView.LayoutManager layoutManager;
    private OrderAdapter orderAdapter;
    private List<OrderClass> donateClassList;
    private OrderClass orderClass;
    private Dialog dialog;


    @BindView(R.id.recycle)
    RecyclerView recyclerView;
    @BindView(R.id.relativeEmpty)
    RelativeLayout relativeEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        getIntentData();
        setToolBar();
        initComponents();
    }

    private void getIntentData() {
        Bundle bundle=getIntent().getExtras();
        if (!bundle.isEmpty()){
            salesmanObject=(Salesman)bundle.get("SalesmanObject");
        }
    }

    private void initComponents() {
        mReference= FirebaseDatabase.getInstance().getReference();
        layoutManager=new LinearLayoutManager(this);
        donateClassList=new ArrayList<>();
        recyclerView.setLayoutManager(layoutManager);
        orderAdapter=new OrderAdapter(donateClassList,this);
        recyclerView.setAdapter(orderAdapter);

        dialog= SharedUtils.ShowWaiting(this,dialog);
    }

    private void ReadOrder(final String text) {
        mReference.child(Constants.Order_Root)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        donateClassList.clear();
                        dialog.show();
                        if (dataSnapshot.exists()){
                            for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                                if (snapshot.child("salesman_UID").getValue().equals(salesmanObject.getSalesman_UID())){
                                    orderClass=snapshot.getValue(OrderClass.class);
                                    if (orderClass.getFullName().trim().contains(text)) {
                                        donateClassList.add(orderClass);
                                    }
                                }
                            }
                        }
                        if (donateClassList.size() != 0) {
                            relativeEmpty.setVisibility(View.GONE);
                        }
                        else {
                            relativeEmpty.setVisibility(View.VISIBLE);
                        }

                        Collections.reverse(donateClassList);
                        orderAdapter.notifyDataSetChanged();
                        dialog.dismiss();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        dialog.dismiss();
                        Toast.makeText(SearchActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void setToolBar(){

        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.bar_search_edit);
        //getSupportActionBar().setElevation(0);
        View view = getSupportActionBar().getCustomView();
        EditText txttitle=view.findViewById(R.id.edt_Search);

        txttitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length()!=0){
                    ReadOrder(s.toString().trim());
                }
            }
        });

        /*setSupportActionBar(toolbar);
        final LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        if (inflator != null) {
            view = inflator.inflate(R.layout.bar_search_edit, null);
        }
        EditText txttitle=view.findViewById(R.id.edt_Search);

        txttitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length()!=0){
                    ReadOrder(s.toString().trim());
                }
            }
        });

        toolbar.addView(view);*/
    }

}
