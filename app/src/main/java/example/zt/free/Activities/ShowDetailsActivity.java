package example.zt.free.Activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import example.zt.free.Classes.OrderClass;
import example.zt.free.R;

public class ShowDetailsActivity extends AppCompatActivity {
    private OrderClass orderClass;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edtName)
    TextView edtName;
    @BindView(R.id.edtPhone)
    TextView edtPhone;
    @BindView(R.id.edtID)
    TextView edtID;
    @BindView(R.id.spinner_IDType)
    TextView spinner_IDType;
    @BindView(R.id.spinner_HomeType)
    TextView spinner_HomeType;
    @BindView(R.id.spinner_Moderia)
    TextView spinner_Moderia;
    @BindView(R.id.edtStreet)
    TextView edtStreet;
    @BindView(R.id.txt_Area)
    TextView txt_Area;
    @BindView(R.id.edtWifeName)
    TextView edtWifeName;
    @BindView(R.id.edtNumber)
    TextView edtNumber;
    @BindView(R.id.txtNotes)
    TextView txtNotes;
    @BindView(R.id.txtNotes2)
    TextView txtNotes2;
    @BindView(R.id.circleImage)
    CircleImageView circleImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_details);
        ButterKnife.bind(this);
        setToolBar();
        initComponents();
        getIntentData();
        showData();
    }

    private void initComponents() {
        //region ImageLoader
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);
        //endregion
    }

    private void showData() {

        ImageLoader.getInstance().displayImage(orderClass.getImagePath(),circleImage,
                new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        circleImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_logo_big));
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        circleImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_logo_big));
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        circleImage.setImageBitmap(loadedImage);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        circleImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_logo_big));
                    }
                });

        edtName.setText(orderClass.getFullName());
        edtPhone.setText(orderClass.getPhone());
        spinner_IDType.setText(orderClass.getID_Type());
        edtID.setText(orderClass.getID_Number());
        spinner_HomeType.setText(orderClass.getHome_Type());
        spinner_Moderia.setText(orderClass.getModeria());
        edtStreet.setText(orderClass.getStreet());
        txt_Area.setText(orderClass.getArea());
        txtNotes2.setText(orderClass.getNotes2());
        edtWifeName.setText(orderClass.getWife_Name());
        edtNumber.setText(orderClass.getNumber());
        txtNotes.setText(orderClass.getNotes());
    }

    private void getIntentData() {
        Bundle bundle=getIntent().getExtras();
        if (!bundle.isEmpty()){
            orderClass=(OrderClass)bundle.get("OrderObject");
        }
    }

    private void setToolBar(){
        setSupportActionBar(toolbar);
        final LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        if (inflator != null) {
            view = inflator.inflate(R.layout.bar_home, null);
        }
        TextView txttitle=view.findViewById(R.id.title);
        TextView txtEdit=view.findViewById(R.id.txtEdit);
        txttitle.setText("تفاصيل الطلب");
        txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getDifferent(orderClass.getOrder_Time())>3){
                    Toast.makeText(ShowDetailsActivity.this, "مرت فترة سماح التعديل", Toast.LENGTH_SHORT).show();
                }else { 
                    Intent intent=new Intent(ShowDetailsActivity.this,EditActivity.class);
                    intent.putExtra("OrderObject",orderClass);
                    startActivityForResult(intent,3015);   
                }
            }
        });
        toolbar.addView(view);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode==3015){
            orderClass=(OrderClass) data.getSerializableExtra("OrderObject");
            showData();
        }
    }

    private long getDifferent(String orderDate){
        long differenceDates=0;
        try {

            Date date1;
            Date date2;

            SimpleDateFormat dates = new SimpleDateFormat("dd-MM-yyyy");

            String CurrentDate = dates.format(Calendar.getInstance().getTime());
            
            //Setting dates
            date1 = dates.parse(CurrentDate);
            date2 = dates.parse(orderDate);





            //Comparing dates
            differenceDates = date1.getTime() - date2.getTime();
            differenceDates = differenceDates / (24 * 60 * 60 * 1000);

            //Convert long to String
            //String dayDifference = Long.toString(differenceDates);

            //Log.e("HERE","HERE: " + dayDifference);

        } catch (Exception exception) {
            Log.e("DIDN'T WORK", "exception " + exception);
        }
        return differenceDates;
    }
}
