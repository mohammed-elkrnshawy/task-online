package example.zt.free.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.zt.free.Classes.AuthClass;
import example.zt.free.R;

public class LoginActivity extends AppCompatActivity {
    private AuthClass authControl;

    @BindView(R.id.btn_login)
    Button btn_login;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    @BindView(R.id.edtPhone)
    EditText edtPhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateData();
            }
        });
    }

    private void validateData() {
        if(TextUtils.isEmpty(edtPhone.getText())){
            edtPhone.setError(getResources().getString(R.string.required));
            edtPhone.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(edtPassword.getText())){
            edtPassword.setError(getResources().getString(R.string.required));
            edtPassword.requestFocus();
            return;
        }

        authControl=new AuthClass(this);
        authControl.Search(edtPhone.getText().toString().trim(),edtPassword.getText().toString().trim());
    }

}
