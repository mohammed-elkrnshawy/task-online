package example.zt.free.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.zt.free.Classes.CameraClass;
import example.zt.free.Classes.Constants;
import example.zt.free.Classes.OrderClass;
import example.zt.free.Classes.SQLEntity;
import example.zt.free.Classes.SQLHelper;
import example.zt.free.Classes.Salesman;
import example.zt.free.Classes.SharedUtils;
import example.zt.free.R;

public class AddActivity extends AppCompatActivity {
    private boolean isFound;
    private Salesman salesmanObject;
    private SimpleDateFormat simpledateformat;
    private CameraClass cameraClass;
    private Dialog dialog;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btnCamera)
    ImageView btnCamera;
    @BindView(R.id.btnGallery)
    ImageView btnGallery;
    @BindView(R.id.imagePhoto)
    ImageView imagePhoto;

    @BindView(R.id.spinner_IDType)
    Spinner spinner_IDType;
    @BindView(R.id.spinner_Moderia)
    Spinner spinner_Moderia;
    @BindView(R.id.spinner_HomeType)
    Spinner spinner_HomeType;

    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.edtPhone)
    EditText edtPhone;
    @BindView(R.id.edtID)
    EditText edtID;
    @BindView(R.id.edtArea)
    EditText edtArea;
    @BindView(R.id.edtStreet)
    EditText edtStreet;
    @BindView(R.id.edtWifeName)
    EditText edtWifeName;
    @BindView(R.id.edtNumber)
    EditText edtNumber;
    @BindView(R.id.txtNotes)
    EditText txtNotes;
    @BindView(R.id.txtNotes2)
    EditText txtNotes2;
    @BindView(R.id.btn_Next)
    Button btn_Next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        ButterKnife.bind(this);
        initComponents();
        setToolBar();
        getIntentData();

        btn_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ValidData();
            }
        });
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraClass.SelectCameraDialog();
            }
        });
        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { cameraClass.SelectGalleryDialog();
            }
        });
    }

    private void initComponents() {
        cameraClass=new CameraClass(this);
    }

    private void setToolBar(){
        setSupportActionBar(toolbar);
        final LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        if (inflator != null) {
            view = inflator.inflate(R.layout.bar_home, null);
        }
        TextView txttitle=view.findViewById(R.id.title);
        txttitle.setText("اضافة الطلب");
        toolbar.addView(view);
    }

    private void getIntentData() {
        Bundle bundle=getIntent().getExtras();
        if (!bundle.isEmpty()){
            salesmanObject=(Salesman)bundle.get("SalesmanObject");
        }
    }

    private void ValidData() {
        if(TextUtils.isEmpty(edtName.getText().toString().trim())){
            edtName.setError(getResources().getString(R.string.required));
            edtName.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(edtPhone.getText().toString().trim())){
            edtPhone.setError(getResources().getString(R.string.required));
            edtPhone.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(edtID.getText().toString().trim())){
            edtID.setError(getResources().getString(R.string.required));
            edtID.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(edtArea.getText().toString().trim())){
            edtArea.setError(getResources().getString(R.string.required));
            edtArea.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(edtStreet.getText().toString().trim())){
            edtStreet.setError(getResources().getString(R.string.required));
            edtStreet.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(edtWifeName.getText().toString().trim())){
            edtWifeName.setError(getResources().getString(R.string.required));
            edtWifeName.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(edtNumber.getText().toString().trim())){
            edtNumber.setError(getResources().getString(R.string.required));
            edtNumber.requestFocus();
            return;
        }

        OrderClass orderClass=new OrderClass();
        orderClass.setFullName(edtName.getText().toString().trim());
        orderClass.setHome_Type(spinner_HomeType.getSelectedItem().toString().trim());
        orderClass.setID_Number(edtID.getText().toString().trim());
        orderClass.setID_Type(spinner_IDType.getSelectedItem().toString().trim());
        orderClass.setModeria(spinner_Moderia.getSelectedItem().toString().trim());
        orderClass.setNotes(txtNotes.getText().toString().trim());
        orderClass.setNumber(edtNumber.getText().toString().trim());
        simpledateformat = new SimpleDateFormat("dd-MM-yyyy");
        orderClass.setOrder_Time(simpledateformat.format(Calendar.getInstance().getTime()));
        orderClass.setPhone(edtPhone.getText().toString());
        orderClass.setSalesman_UID(salesmanObject.getSalesman_UID());
        orderClass.setSalesman_Name(salesmanObject.getSalesman_Name());
        orderClass.setStreet(edtStreet.getText().toString());
        orderClass.setWife_Name(edtWifeName.getText().toString());
        orderClass.setArea(edtArea.getText().toString().trim());
        orderClass.setNotes2(txtNotes2.getText().toString().trim());

        dialog= SharedUtils.ShowWaiting(this,dialog);
        dialog.show();
        String Key=FirebaseDatabase.getInstance().getReference().child(Constants.Order_Root).push().getKey();
        orderClass.setOrder_UID(Key);
        orderClass.setOrder_ID((Constants.order_ID+1));


        if (isNetworkConnected()){
            checkNationalId(orderClass);
        }else {
            saveOffline(orderClass);
        }
        



    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private void saveOnline(OrderClass orderClass){
        cameraClass.uploadPhoto(orderClass);
    }

    private void checkNationalId(final OrderClass orderClass) {
        FirebaseDatabase.getInstance().getReference().child(Constants.Order_Root)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                                if (snapshot.child("id_Number").getValue().equals(orderClass.getID_Number())){
                                    isFound=true;
                                    break;
                                }
                            }
                        }else {
                            isFound=false;
                        }
                        
                        if (!isFound){
                            saveOnline(orderClass);
                        }
                        else {
                            Toast.makeText(AddActivity.this, "رقم الهوية مسجل مسبقا", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void saveOffline(OrderClass orderClass){
        SQLHelper dbHelper = new SQLHelper(this);

        SQLiteDatabase db = dbHelper.getWritableDatabase();

// Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(SQLEntity.COLUMN_NAME_fullName, orderClass.getFullName());
        values.put(SQLEntity.COLUMN_NAME_home_Type, orderClass.getHome_Type());
        values.put(SQLEntity.COLUMN_NAME_id_Number, orderClass.getID_Number());
        values.put(SQLEntity.COLUMN_NAME_id_Type, orderClass.getID_Type());
        values.put(SQLEntity.COLUMN_NAME_moderia, orderClass.getModeria());
        values.put(SQLEntity.COLUMN_NAME_notes, orderClass.getNotes());
        values.put(SQLEntity.COLUMN_NAME_number, orderClass.getNumber());
        values.put(SQLEntity.COLUMN_NAME_order_Time, orderClass.getOrder_Time());
        values.put(SQLEntity.COLUMN_NAME_phone, orderClass.getPhone());
        values.put(SQLEntity.COLUMN_NAME_street, orderClass.getStreet());
        values.put(SQLEntity.COLUMN_NAME_wife_Name, orderClass.getWife_Name());
        values.put(SQLEntity.COLUMN_NAME_area, orderClass.getArea());
        values.put(SQLEntity.COLUMN_NAME_notes2, orderClass.getNotes2());


// Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(SQLEntity.TABLE_NAME, null, values);
        dialog.dismiss();
        finish();
        Toast.makeText(this, "انت غير متصل بالانترنت ... تم حفظ البيانات محليا", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode== Activity.RESULT_OK&&(requestCode==Constants.Camera||requestCode==Constants.Gallery)){
            cameraClass.onPhotoResult(requestCode,resultCode,data,imagePhoto);
        }
    }
}
